package com.betca.practica.services;

import com.betca.practica.documents.core.Role;
import com.betca.practica.documents.core.User;
import com.betca.practica.repositories.UserRepository;
import com.betca.practica.resources.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {

    public static final String P_TOKEN = "";

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(final String usernameOrTokenValue) {
        User user;
        Optional<User> optionalUser = userRepository.findByTokenValue(usernameOrTokenValue);
        if (optionalUser.isPresent()) {
            user = optionalUser.get();
            return this.userBuilder(user.getUsername(), new BCryptPasswordEncoder().encode(P_TOKEN), user.getRoles());
        }

        optionalUser = userRepository.findByUsername(usernameOrTokenValue);
        if (optionalUser.isPresent()) {
            user = optionalUser.get();
            return this.userBuilder(user.getUsername(), user.getPassword(), user.getRoles());
        }

        throw new NotFoundException();
    }

    private org.springframework.security.core.userdetails.User userBuilder(String username, String password, Role[] roles) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (Role role : roles) {
            authorities.add(new SimpleGrantedAuthority(role.roleName()));
        }
        return new org.springframework.security.core.userdetails.User(username, password, authorities);
    }
}
