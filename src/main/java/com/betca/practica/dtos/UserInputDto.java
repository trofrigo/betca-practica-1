package com.betca.practica.dtos;

import com.betca.practica.documents.core.Role;
import com.betca.practica.documents.core.User;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@ToString
public class UserInputDto {

    @NotNull
    private @Getter @Setter String username;

    private @Getter @Setter String password;

    private @Getter @Setter Role[] roles;

    public UserInputDto() {
        //Required by Spring
    }

    public UserInputDto(String username) {
        this.username = username;
    }


    public UserInputDto(User user) {
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.roles = user.getRoles();
    }
}
