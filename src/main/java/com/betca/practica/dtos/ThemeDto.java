package com.betca.practica.dtos;


import com.betca.practica.documents.core.Theme;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.Date;

@ToString
@EqualsAndHashCode(exclude = {
        "name",
        "date"
})
public class ThemeDto {

    private @Getter @Setter String id;

    @NotNull
    private @Getter @Setter String name;

    private @Getter @Setter Date date;

    public ThemeDto(){
        // Required by Spring
    }

    public ThemeDto(String name){
        this.name = name;
    }

    public ThemeDto(Theme theme){
        this.id = theme.getId();
        this.name = theme.getName();
        this.date = theme.getDate();
    }


}
