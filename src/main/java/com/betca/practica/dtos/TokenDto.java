package com.betca.practica.dtos;

import com.betca.practica.documents.core.Role;
import com.betca.practica.documents.core.User;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public class TokenDto {

    private @Getter @Setter String token;

    private @Getter @Setter Role[] roles;

    public TokenDto() {
        //Required by Spring
    }

    public TokenDto(User user) {
        this.token = user.getToken().getValue();
        this.roles = user.getRoles();
    }
}
