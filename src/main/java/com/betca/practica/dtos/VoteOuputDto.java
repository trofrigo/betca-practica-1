package com.betca.practica.dtos;


import com.betca.practica.documents.core.Theme;
import com.betca.practica.documents.core.User;
import com.betca.practica.documents.core.Vote;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@ToString
@EqualsAndHashCode(exclude = {
    "value",
    "theme"
})
public class VoteOuputDto {

    private @Getter @Setter String id;

    private @Getter @Setter int value;

    private @Getter @Setter Theme theme;

    public VoteOuputDto(){
        // Required by Spring
    }

    public VoteOuputDto(Vote vote){
        this.id = vote.getId();
        this.value = vote.getValue();
        this.theme = vote.getTheme();
    }

}
