package com.betca.practica.dtos;


import com.betca.practica.documents.core.Theme;
import com.betca.practica.documents.core.Vote;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@ToString
@EqualsAndHashCode(exclude = {
    "value",
    "themeId"
})
public class VoteInputDto {

    @NotNull
    @Max(10)
    @Min(0)
    private @Getter @Setter int value;

    @NotNull
    private @Getter @Setter String themeId;

    public VoteInputDto(){
        // Required by Spring
    }

    public VoteInputDto(int value, String themeId){
        this.value = value;
        this.themeId = themeId;
    }


    public VoteInputDto(Vote vote){
        this.value = vote.getValue();
        this.themeId = vote.getTheme().getId();
    }

}
