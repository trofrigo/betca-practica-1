package com.betca.practica.dtos;

import com.betca.practica.documents.core.Role;
import com.betca.practica.documents.core.User;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@ToString
public class UserOutputDto {

    @NotNull
    private @Getter @Setter String username;

    private @Getter @Setter Role[] roles;

    public UserOutputDto() {
        //Required by Spring
    }

    public UserOutputDto(String username) {
        this.username = username;
    }


    public UserOutputDto(User user) {
        this.username = user.getUsername();
        this.roles = user.getRoles();
    }
}
