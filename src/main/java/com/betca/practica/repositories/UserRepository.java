package com.betca.practica.repositories;

import com.betca.practica.documents.core.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Optional;

public interface UserRepository extends MongoRepository<User, String> {

    public Optional<User> findByUsername(String username);

    @Query("{ 'token.value' : ?0 }")
    public Optional<User> findByTokenValue(String tokenValue);

}
