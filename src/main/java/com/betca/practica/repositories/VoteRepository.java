package com.betca.practica.repositories;

import com.betca.practica.documents.core.Theme;
import com.betca.practica.documents.core.User;
import com.betca.practica.documents.core.Vote;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface VoteRepository extends MongoRepository<Vote, String>{

    public List<Vote> findAllByTheme(Theme theme);

    public List<Vote> findAllByUser(User user);

    public void deleteAllByTheme(Theme theme);

    public void deleteAllByUser(User user);

}
