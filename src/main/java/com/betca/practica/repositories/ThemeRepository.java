package com.betca.practica.repositories;

import com.betca.practica.documents.core.Theme;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface ThemeRepository extends MongoRepository<Theme, String>{

    public Optional<Theme> findByName(String name);
}
