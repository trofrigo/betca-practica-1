package com.betca.practica.resources;

import com.betca.practica.controllers.ThemeController;
import com.betca.practica.dtos.ThemeDto;
import com.betca.practica.resources.exceptions.ConflictException;
import com.betca.practica.resources.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@PreAuthorize("hasRole('ADMIN')")
@RestController
@RequestMapping(ThemeResource.THEMES)
public class ThemeResource {

    public static final String THEMES = "/themes";

    public static final String ID = "/{id}";

    @Autowired
    public ThemeController themeController;

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @GetMapping()
    public List<ThemeDto> getAllThemes(){
        return this.themeController.getAllThemes();
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    @GetMapping(ID)
    public ThemeDto getTheme(@PathVariable() String id){
        return this.themeController.getTheme(id)
                .orElseThrow(NotFoundException::new);
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public void createTheme(@Valid @RequestBody() ThemeDto themeDto){
        if(this.themeController.themeNameIsUsed(themeDto.getName())){
            throw new ConflictException();
        }
        this.themeController.createTheme(themeDto);
    }

    @PutMapping(ID)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateTheme(@PathVariable String id, @Valid @RequestBody ThemeDto themeDto){
        if(this.themeController.themeNameIsUsed(themeDto.getName())){
            throw new ConflictException();
        }
        this.themeController.updateTheme(id, themeDto)
                .orElseThrow(NotFoundException::new);

    }

    @DeleteMapping(ID)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTheme(@PathVariable String id){
        this.themeController.deleteTheme(id);
    }
}
