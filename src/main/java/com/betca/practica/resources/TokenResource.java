package com.betca.practica.resources;

import com.betca.practica.controllers.TokenController;
import com.betca.practica.dtos.TokenDto;
import com.betca.practica.dtos.UserInputDto;
import com.betca.practica.resources.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(TokenResource.TOKENS)
public class TokenResource {

    public static final String TOKENS = "/tokens";

    public static final String AUTHENTICATED = "/authenticated";

    @Autowired
    private TokenController tokenController;

    @PostMapping
    public TokenDto login(@AuthenticationPrincipal User activeUser) {
        return tokenController.login(activeUser.getUsername())
                .orElseThrow(NotFoundException::new);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(value = AUTHENTICATED)
    public boolean tokenRoles(@AuthenticationPrincipal User activeUser) {
        return true;
    }
}
