package com.betca.practica.resources;

import com.betca.practica.controllers.UserController;
import com.betca.practica.documents.core.Role;
import com.betca.practica.dtos.UserInputDto;
import com.betca.practica.resources.exceptions.ConflictException;
import com.betca.practica.resources.exceptions.ForbiddenException;
import com.betca.practica.resources.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@PreAuthorize("hasRole('ADMIN')")
@RestController
@RequestMapping(UserResource.USERS)
public class UserResource {

    public static final String USERS = "/users";

    public static final String USERNAME = "/{username}";

    @Autowired
    private UserController userController;

    @GetMapping(value = USERNAME)
    public UserInputDto getUser(@PathVariable String username) {
        return this.userController.getUser(username)
                .orElseThrow(NotFoundException::new);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createUser(@Valid @RequestBody UserInputDto userInputDto) {
        String username = userInputDto.getUsername();

        if(this.userController.usernameExists(username)){
            throw new ConflictException();
        }

        this.userController.createUser(userInputDto, new Role[] {Role.USER});
    }

    @PutMapping(value = USERNAME)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateUser(@PathVariable String username, @Valid @RequestBody UserInputDto userInputDto) {
        if(!this.userController.usernameExists(username)){
            throw new NotFoundException();
        }

        if(!this.userController.actionIsAllowed(username, new Role[] {Role.USER})){
            throw new ForbiddenException();
        }

        this.userController.updateUser(username, userInputDto);
    }

    @DeleteMapping(value = USERNAME)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable String username) {
        if(!this.userController.usernameExists(username)){
            throw new NotFoundException();
        }

        if(!this.userController.actionIsAllowed(username, new Role[] {Role.USER})){
            throw new ForbiddenException();
        }

        this.userController.deleteUser(username);
    }
}
