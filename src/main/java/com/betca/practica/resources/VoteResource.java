package com.betca.practica.resources;

import com.betca.practica.controllers.ThemeController;
import com.betca.practica.controllers.UserController;
import com.betca.practica.controllers.VoteController;
import com.betca.practica.dtos.VoteInputDto;
import com.betca.practica.dtos.VoteOuputDto;
import com.betca.practica.resources.exceptions.BadRequestException;
import com.betca.practica.resources.exceptions.ForbiddenException;
import com.betca.practica.resources.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
@RestController
@RequestMapping(VoteResource.VOTES)
public class VoteResource {

    public static final String VOTES = "/votes";

    public static final String ID = "/{id}";

    @Autowired
    public VoteController voteController;

    @Autowired
    public ThemeController themeController;

    @Autowired
    public UserController userController;

    @GetMapping
    public List<VoteOuputDto> getAllVotes(@AuthenticationPrincipal User activeUser){
        return this.voteController.getAllVotes(activeUser.getUsername());
    }

    @GetMapping(ID)
    public VoteOuputDto getVote(@AuthenticationPrincipal User activeUser,
                                @PathVariable String id){
        if(!this.voteController.ownsVote(activeUser.getUsername(), id)){
            throw new ForbiddenException();
        }

        return this.voteController.getVote(id)
                .orElseThrow(NotFoundException::new);
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public void createVote(@AuthenticationPrincipal User activeUser,
                           @Valid @RequestBody() VoteInputDto voteDto){
        if(!this.themeController.themeExists(voteDto.getThemeId())){
            throw new BadRequestException();
        }

        this.voteController.createVote(activeUser.getUsername(), voteDto);
    }

    @PutMapping(ID)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateVote(@AuthenticationPrincipal User activeUser,
                           @PathVariable String id,
                           @Valid @RequestBody VoteInputDto voteDto){

        if(!this.voteController.ownsVote(activeUser.getUsername(), id)){
            throw new ForbiddenException();
        }

        if(!this.themeController.themeExists(voteDto.getThemeId())){
            throw new BadRequestException();
        }

        this.voteController.updateVote(id, voteDto)
                .orElseThrow(NotFoundException::new);
    }

    @DeleteMapping(ID)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteVote(@AuthenticationPrincipal User activeUser,
                           @PathVariable String id){

        if(!this.voteController.ownsVote(activeUser.getUsername(), id)){
            throw new ForbiddenException();
        }

        this.voteController.deleteVote(id);
    }



}
