package com.betca.practica.controllers;

import com.betca.practica.documents.core.Role;
import com.betca.practica.documents.core.User;
import com.betca.practica.dtos.UserInputDto;
import com.betca.practica.dtos.UserOutputDto;
import com.betca.practica.repositories.UserRepository;
import com.betca.practica.repositories.VoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.Optional;

@Controller
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private VoteRepository voteRepository;

    public Optional<UserInputDto> getUser(String username) {
        Optional<User> optionalUser = this.userRepository.findByUsername(username);
        return optionalUser.map(UserInputDto::new);
    }

    public void createUser(UserInputDto userInputDto, Role[] defaultRoles) {
        User user = new User(userInputDto.getUsername(), userInputDto.getPassword());
        user.setRoles(defaultRoles);
        this.userRepository.save(user);
    }

    public Optional<UserOutputDto> updateUser(String username, UserInputDto userInputDto) {
        Optional<User> optionalUser = this.userRepository.findByUsername(username);
        if(optionalUser.isPresent()) {
            User user = optionalUser.get();
            user.setUsername(userInputDto.getUsername());
            this.userRepository.save(user);
            return Optional.of(new UserOutputDto(user));
        } else {
            return Optional.empty();
        }
    }

    public void deleteUser(String username) {
        Optional<User> optionalUser = this.userRepository.findByUsername(username);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            this.voteRepository.deleteAllByUser(user);
            this.userRepository.delete(user);
        }
    }

    public boolean usernameExists(@Valid String username) {
        Optional<User> optionalUser = this.userRepository.findByUsername(username);
        return optionalUser.isPresent();
    }

    public boolean actionIsAllowed(String username, Role[] allowedRoles){
        Optional<User> optionalUser = this.userRepository.findByUsername(username);
        if(optionalUser.isPresent()){
            return Arrays.asList(allowedRoles).containsAll(Arrays.asList(optionalUser.get().getRoles()));
        } else {
            return false;
        }
    }
}
