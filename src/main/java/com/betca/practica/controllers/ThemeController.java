package com.betca.practica.controllers;

import com.betca.practica.documents.core.Theme;
import com.betca.practica.dtos.ThemeDto;
import com.betca.practica.repositories.ThemeRepository;
import com.betca.practica.repositories.VoteRepository;
import jdk.nashorn.internal.runtime.options.Option;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
public class ThemeController {
    @Autowired
    public ThemeRepository themeRepository;

    @Autowired
    public VoteRepository voteRepository;

    public List<ThemeDto> getAllThemes() {
        return this.themeRepository.findAll()
                .stream()
                .map(ThemeDto::new)
                .collect(Collectors.toList());
    }

    public Optional<ThemeDto> getTheme(String id) {
        Optional<Theme> optionalTheme = this.themeRepository.findById(id);
        return optionalTheme.map(ThemeDto::new);
    }

    public void createTheme(ThemeDto themeDto) {
        Theme theme = new Theme(themeDto);
        this.themeRepository.save(theme);
    }

    public Optional<ThemeDto> updateTheme(String id, ThemeDto themeDto) {
        Optional<Theme> optionalTheme = this.themeRepository.findById(id);

        if(optionalTheme.isPresent()){

            Theme theme = optionalTheme.get();

            if(themeDto.getName() != null){
                theme.setName(themeDto.getName());
            }

            if(themeDto.getDate() != null){
                theme.setDate(themeDto.getDate());
            }

            this.themeRepository.save(theme);
            return Optional.of(new ThemeDto(theme));
        } else {
            return Optional.empty();
        }
    }

    public void deleteTheme(String id) {
        Optional<Theme> optionalTheme = this.themeRepository.findById(id);
        optionalTheme.ifPresent(theme -> {
            this.voteRepository.deleteAllByTheme(theme);
            this.themeRepository.delete(theme);
        });
    }

    public boolean themeExists(String id){
        Optional<Theme> optionalTheme = this.themeRepository.findById(id);
        return optionalTheme.isPresent();
    }

    public boolean themeNameIsUsed(String name){
        Optional<Theme> optionalTheme = this.themeRepository.findByName(name);
        return optionalTheme.isPresent();
    }
}
