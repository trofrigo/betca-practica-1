package com.betca.practica.controllers;

import com.betca.practica.documents.core.Theme;
import com.betca.practica.documents.core.User;
import com.betca.practica.documents.core.Vote;
import com.betca.practica.dtos.VoteInputDto;
import com.betca.practica.dtos.VoteOuputDto;
import com.betca.practica.repositories.ThemeRepository;
import com.betca.practica.repositories.UserRepository;
import com.betca.practica.repositories.VoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller

public class VoteController {

    @Autowired
    public VoteRepository voteRepository;

    @Autowired
    public ThemeRepository themeRepository;

    @Autowired
    public UserRepository userRepository;

    public List<VoteOuputDto> getAllVotes(String username) {
        Optional<User> optionalUser = this.userRepository.findByUsername(username);
        List<Vote> votes;
        if (optionalUser.isPresent()) {
            votes = this.voteRepository.findAllByUser(optionalUser.get());
        }else {
            votes = this.voteRepository.findAll();
        }
        return votes
                .stream()
                .map(VoteOuputDto::new)
                .collect(Collectors.toList());
    }

    public Optional<VoteOuputDto> getVote(String id) {
        Optional<Vote> optionalVote = this.voteRepository.findById(id);
        return optionalVote.map(VoteOuputDto::new);
    }

    public void createVote(String username, VoteInputDto voteDto) {
        Optional<Theme> theme = this.themeRepository.findById(voteDto.getThemeId());
        Optional<User> user = this.userRepository.findByUsername(username);

        if(theme.isPresent() && user.isPresent()){
            Vote vote = new Vote(voteDto.getValue(), theme.get(), user.get());
            this.voteRepository.save(vote);
        }
    }

    public Optional<VoteOuputDto> updateVote(String id, VoteInputDto voteDto) {
        Optional<Vote> optionalVote = this.voteRepository.findById(id);
        Optional<Theme> theme = this.themeRepository.findById(voteDto.getThemeId());

        if(optionalVote.isPresent() && theme.isPresent()){

            Vote vote = optionalVote.get();
            vote.setValue(voteDto.getValue());
            vote.setTheme(theme.get());

            this.voteRepository.save(vote);
            return Optional.of(new VoteOuputDto(vote));
        } else {
            return Optional.empty();
        }
    }

    public void deleteVote(String id) {
        Optional<Vote> optionalVote = this.voteRepository.findById(id);
        optionalVote.ifPresent(vote -> this.voteRepository.delete(vote));
    }

    public boolean ownsVote(String username, String voteId) {
        Optional<Vote> optionalVote = this.voteRepository.findById(voteId);
        return optionalVote
                .map(vote -> vote.getUser().getUsername().equals(username))
                .orElse(false);
    }
}
