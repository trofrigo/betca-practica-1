package com.betca.practica.controllers;

import com.betca.practica.documents.core.Token;
import com.betca.practica.documents.core.User;
import com.betca.practica.dtos.TokenDto;
import com.betca.practica.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.Optional;

@Controller
public class TokenController {

    @Autowired
    private UserRepository userRepository;

    public Optional<TokenDto> login(String username) {
        Optional<User> optionalUser = userRepository.findByUsername(username);
        if(optionalUser.isPresent()){
            User user = optionalUser.get();
            user.setToken(new Token());
            userRepository.save(user);
            return Optional.of(new TokenDto(user));
        }else{
            return Optional.empty();
        }
    }
}
