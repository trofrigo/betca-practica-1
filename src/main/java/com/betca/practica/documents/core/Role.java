package com.betca.practica.documents.core;

public enum Role {
    ADMIN, USER;

    public String roleName() {
        return "ROLE_" + this.toString();
    }

}
