package com.betca.practica.documents.core;

import com.betca.practica.dtos.VoteOuputDto;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@ToString
@EqualsAndHashCode(exclude = {
    "value",
    "theme"
})
public class Vote {

    private @Getter String id;

    private @Getter @Setter int value;

    @DBRef
    private @Getter @Setter Theme theme;

    @DBRef
    private @Getter @Setter User user;

    public Vote(){
        //Requires by Spring
    }

    public Vote(int value, Theme theme, User user){
        setValue(value);
        setTheme(theme);
        setUser(user);
    }
}
