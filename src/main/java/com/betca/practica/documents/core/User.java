package com.betca.practica.documents.core;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.validation.constraints.NotNull;

@Document
@ToString
@EqualsAndHashCode(exclude = {
        "password",
        "token",
        "roles"
})
public class User {

    @Id
    private @Getter String id;

    @Indexed(unique = true)
    private @Getter @Setter String username;

    private @Getter String password;

    private @Getter @Setter Role[] roles;

    private @Getter @Setter Token token;

    public User(){
        //Required by Spring
    }

    public User(@NotNull String username, String password) {
        setUsername(username);
        setPassword(password);
        this.setRoles(new Role[]{Role.USER});
    }

    public void setPassword(String password) {
        this.password = new BCryptPasswordEncoder().encode(password);
    }

}
