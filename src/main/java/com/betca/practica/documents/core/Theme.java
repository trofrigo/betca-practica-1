package com.betca.practica.documents.core;

import com.betca.practica.dtos.ThemeDto;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Calendar;
import java.util.Date;

@Document
@ToString
@EqualsAndHashCode(exclude = {
    "name",
    "date"
})
public class Theme {

    @Id
    private @Getter String id;

    private @Getter @Setter String name;

    private @Getter @Setter Date date;

    public Theme(){
        //Required by Spring
    }

    public Theme(String name){
        this.name = name;
        this.date = Calendar.getInstance().getTime();
    }

    public Theme(ThemeDto themeDto){
        this.name = themeDto.getName();
        this.date = themeDto.getDate() != null
            ? themeDto.getDate()
            : Calendar.getInstance().getTime();
    }
}
