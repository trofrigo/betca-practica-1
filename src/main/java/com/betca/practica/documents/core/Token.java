package com.betca.practica.documents.core;

import com.betca.practica.utils.Encrypting;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
@ToString
@EqualsAndHashCode()
public class Token {

    private @Getter String value;

    private @Getter Date creationDate;

    public Token() {
        this.setValue(new Encrypting().encryptInBase64UrlSafe());
    }

    public void setValue(String value) {
        this.value = value;
        this.creationDate = new Date();
    }
}
