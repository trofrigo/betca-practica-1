package com.betca.practica;

import com.betca.practica.repositories.RepositoriesTestsAggregator;
import com.betca.practica.resources.ResourcesTestsAggregator;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
    RepositoriesTestsAggregator.class,
    ResourcesTestsAggregator.class,
})
public class TestsAggregator {
}
