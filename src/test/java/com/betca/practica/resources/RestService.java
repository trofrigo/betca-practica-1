package com.betca.practica.resources;

import com.betca.practica.documents.core.Role;
import com.betca.practica.documents.core.User;
import com.betca.practica.dtos.TokenDto;
import com.betca.practica.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class RestService {

    @Autowired
    private Environment environment;

    @Value("${server.servlet.context-path}")
    private String contextPath;

    @Autowired
    private UserRepository userRepository;

    private TokenDto tokenDto;

    private int port() {
        return Integer.parseInt(environment.getProperty("local.server.port"));
    }

    public <T> RestBuilder<T> restBuilder(RestBuilder<T> restBuilder) {
        restBuilder.port(this.port());
        restBuilder.path(contextPath);
        if (tokenDto != null) {
            restBuilder.basicAuth(tokenDto.getToken());
        }
        return restBuilder;
    }

    public RestBuilder<Object> restBuilder() {
        RestBuilder<Object> restBuilder = new RestBuilder<>(this.port());
        restBuilder.path(contextPath);
        if (tokenDto != null) {
            restBuilder.basicAuth(tokenDto.getToken());
        }
        return restBuilder;
    }

    public RestService logout() {
        this.tokenDto = null;
        return this;
    }

    public RestService loginAdmin() {
        String username = "admin";
        String password = "admin";
        User admin = new User(username, password);
        admin.setRoles(new Role[]{Role.ADMIN, Role.USER});
        addUser(admin);

        return login(username, password);
    }

    public RestService login(String username, String password) {
        this.tokenDto = new RestBuilder<TokenDto>(this.port()).path(contextPath).path(TokenResource.TOKENS)
                .basicAuth(username, password).clazz(TokenDto.class).post().build();
        return this;
    }

    private void addUser(User user){
        this.userRepository.save(user);
    }
}
