package com.betca.practica.resources;

import com.betca.practica.documents.core.Theme;
import com.betca.practica.dtos.ThemeDto;
import com.betca.practica.repositories.ThemeRepository;
import com.betca.practica.repositories.UserRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:test.properties")
public class ThemeResourceFunctionalTesting {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Autowired
    private RestService restService;

    @Autowired
    private ThemeRepository themeRepository;

    @Autowired
    private UserRepository userRepository;

    private ThemeDto themeDto;

    @Before
    public void init(){
        //Cleaning database
        this.themeRepository.deleteAll();
        this.userRepository.deleteAll();
        this.themeDto = new ThemeDto("Tema de prueba");
    }

    public void addTheme(Theme theme){
        this.themeRepository.save(theme);
    }

    @Test
    public void createThemeTest(){
        this.restService.loginAdmin()
                .restBuilder().path(ThemeResource.THEMES)
                .body(this.themeDto).post().build();
    }

    @Test
    public void createThemeBadRequestTest(){
        thrown.expect(new HttpMatcher(HttpStatus.UNAUTHORIZED));
        this.themeDto.setName(null);
        this.restService.restBuilder().path(ThemeResource.THEMES).body(this.themeDto).post().build();
    }

    @Test
    public void updateThemeTest(){
        Theme theme = new Theme(this.themeDto);
        addTheme(theme);
        this.themeDto.setName("Tema de prueba 2");
        this.restService.loginAdmin()
                .restBuilder().path(ThemeResource.THEMES)
                .path(ThemeResource.ID).expand(theme.getId())
                .body(this.themeDto).put().build();
    }

    @Test
    public void updateThemeBadQuestTest(){
        Theme theme = new Theme(this.themeDto);
        addTheme(theme);

        thrown.expect(new HttpMatcher(HttpStatus.BAD_REQUEST));
        this.themeDto.setName(null);
        this.restService.loginAdmin()
                .restBuilder().path(ThemeResource.THEMES)
                .path(ThemeResource.ID).expand(theme.getId())
                .body(this.themeDto).put().build();
    }

    @Test
    public void deleteThemeTest(){
        Theme theme = new Theme(this.themeDto);
        addTheme(theme);
        this.restService.loginAdmin().restBuilder().path(ThemeResource.THEMES)
                .path(ThemeResource.ID).expand(theme.getId())
                .delete().build();
    }

    @Test
    public void deleteNotExistingThemeTest(){
        //Since delete must an idempotent method, it should no fail if the theme doesn't exist
        this.restService.loginAdmin().restBuilder().path(ThemeResource.THEMES)
                .path(ThemeResource.ID).expand(new Random())
                .delete().build();
    }
}
