package com.betca.practica.resources;


import com.betca.practica.documents.core.Role;
import com.betca.practica.documents.core.Theme;
import com.betca.practica.documents.core.User;
import com.betca.practica.documents.core.Vote;
import com.betca.practica.dtos.ThemeDto;
import com.betca.practica.dtos.UserInputDto;
import com.betca.practica.dtos.VoteInputDto;
import com.betca.practica.repositories.ThemeRepository;
import com.betca.practica.repositories.UserRepository;
import com.betca.practica.repositories.VoteRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:test.properties")
public class VoteResourceFunctionalTesting {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Autowired
    private RestService restService;

    @Autowired
    private ThemeRepository themeRepository;

    @Autowired
    private VoteRepository voteRepository;

    @Autowired
    private UserRepository userRepository;

    private Theme theme;

    private User user;

    private String username;

    private String password;

    private VoteInputDto voteInputDto;

    @Before
    public void init(){
        //Cleaning database
        this.themeRepository.deleteAll();
        this.userRepository.deleteAll();
        this.voteRepository.deleteAll();

        this.theme = new Theme(new ThemeDto("Tema de prueba"));

        addTheme(this.theme);

        this.username = "user";
        this.password = "user";
        this.user = new User(this.username, this.password);

        addUser(this.user);

        this.voteInputDto = new VoteInputDto(5, this.theme.getId());
    }

    private void addTheme(Theme theme){
        this.themeRepository.save(theme);
    }

    private void addVote(Vote vote){
        this.voteRepository.save(vote);
    }

    private void addUser(User user) {
        this.userRepository.save(user);
    }

    @Test
    public void createVote(){
        this.restService.loginAdmin().restBuilder().path(VoteResource.VOTES).body(this.voteInputDto).post().build();
    }

    @Test
    public void createVoteBadValueTest(){
        thrown.expect(new HttpMatcher(HttpStatus.BAD_REQUEST));
        VoteInputDto voteInputDto = new VoteInputDto();
        this.restService.loginAdmin().restBuilder().path(VoteResource.VOTES).body(voteInputDto).post().build();
    }

    @Test
    public void updateVoteTest(){
        Vote vote = new Vote(5, this.theme, this.user);
        addVote(vote);

        VoteInputDto voteInputDto = new VoteInputDto(vote);
        voteInputDto.setValue(7);
        this.restService.login(this.username, this.password)
                .restBuilder().path(VoteResource.VOTES)
                .path(VoteResource.ID).expand(vote.getId())
                .body(voteInputDto).put().build();
    }

    @Test
    public void updateVoteBadRequestTest(){
        Vote vote = new Vote(5, this.theme, this.user);
        addVote(vote);

        thrown.expect(new HttpMatcher(HttpStatus.BAD_REQUEST));
        VoteInputDto voteInputDto = new VoteInputDto(vote);
        voteInputDto.setValue(12);
        this.restService.loginAdmin().restBuilder().path(VoteResource.VOTES)
                .path(VoteResource.ID).expand(vote.getId())
                .body(voteInputDto).put().build();
    }

    @Test
    public void deleteVoteTest(){
        Vote vote = new Vote(5, this.theme, this.user);
        addVote(vote);

        this.restService.login(this.username, this.password)
                .restBuilder().path(VoteResource.VOTES)
                .path(VoteResource.ID).expand(vote.getId())
                .delete().build();
    }

    @Test
    public void deleteNotExistingVoteTest(){
        thrown.expect(new HttpMatcher(HttpStatus.FORBIDDEN));
        this.restService.login(this.username, this.password)
                .restBuilder().path(VoteResource.VOTES)
                .path(VoteResource.ID).expand(new Random())
                .delete().build();
    }
}
