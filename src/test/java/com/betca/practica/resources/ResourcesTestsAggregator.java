package com.betca.practica.resources;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
        VoteResourceFunctionalTesting.class,
        ThemeResourceFunctionalTesting.class,
        UserResourcesFunctionalTesting.class,
})
public class ResourcesTestsAggregator {

}
