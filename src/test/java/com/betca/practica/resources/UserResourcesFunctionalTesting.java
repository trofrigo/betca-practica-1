package com.betca.practica.resources;

import com.betca.practica.documents.core.Role;
import com.betca.practica.documents.core.User;
import com.betca.practica.dtos.UserInputDto;
import com.betca.practica.repositories.UserRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:test.properties")
public class UserResourcesFunctionalTesting {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Autowired
    private RestService restService;

    @Autowired
    private UserRepository userRepository;

    @Before
    public void init(){
        //Cleaning database
        this.userRepository.deleteAll();
    }

    public void addUser(User user){
        user.setRoles(new Role[] {Role.USER});
        this.userRepository.save(user);
    }

    @Test
    public void getUserTest(){
        User user = new User("user", "user");
        addUser(user);

        this.restService.loginAdmin()
                .restBuilder()
                .path(UserResource.USERS)
                .path(UserResource.USERNAME).expand(user.getUsername())
                .get().build();
    }

    @Test
    public void getUserBadRequestTest(){
        thrown.expect(new HttpMatcher(HttpStatus.NOT_FOUND));
        this.restService.loginAdmin()
                .restBuilder()
                .path(UserResource.USERS)
                .path(UserResource.USERNAME).expand(new Random())
                .get().build();
    }

    @Test
    public void createUserTest(){
        User user = new User("user", "user");

        this.restService.loginAdmin()
                .restBuilder()
                .path(UserResource.USERS)
                .body(user)
                .post().build();
    }

    @Test
    public void createUserAlreadyExistsTest(){
        User user = new User("user", "user");
        addUser(user);

        thrown.expect(new HttpMatcher(HttpStatus.CONFLICT));
        this.restService.loginAdmin()
                .restBuilder()
                .path(UserResource.USERS)
                .body(new UserInputDto(user))
                .post().build();
    }

    @Test
    public void updateUserTest(){
        User user = new User("user", "user");
        addUser(user);

        user.setPassword("password");
        this.restService.loginAdmin()
                .restBuilder()
                .path(UserResource.USERS)
                .path(UserResource.USERNAME).expand(user.getUsername())
                .body(new UserInputDto(user))
                .put().build();
    }

    @Test
    public void updateUserDoesNotExistTest(){
        User user = new User("user", "user");

        thrown.expect(new HttpMatcher(HttpStatus.NOT_FOUND));
        user.setPassword("password");
        this.restService.loginAdmin()
                .restBuilder()
                .path(UserResource.USERS)
                .path(UserResource.USERNAME).expand(user.getUsername())
                .body(new UserInputDto(user))
                .put().build();
    }

    @Test
    public void deleteUserTest(){
        User user = new User("user", "user");
        addUser(user);

        this.restService.loginAdmin()
                .restBuilder()
                .path(UserResource.USERS)
                .path(UserResource.USERNAME).expand(user.getUsername())
                .delete().build();
    }
}
