package com.betca.practica.repositories;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
    VoteRespositoryIT.class,
})
public class RepositoriesTestsAggregator {

}
