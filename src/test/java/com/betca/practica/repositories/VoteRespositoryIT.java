package com.betca.practica.repositories;


import com.betca.practica.documents.core.Theme;
import com.betca.practica.documents.core.User;
import com.betca.practica.documents.core.Vote;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:test.properties")
public class VoteRespositoryIT {

    @Autowired
    private VoteRepository voteRepository;

    @Autowired
    private ThemeRepository themeRepository;

    @Autowired
    private UserRepository userRepository;

    private Vote vote;

    private Theme theme;

    private User user;


    @Before
    public void init(){
        this.theme = new Theme("Test Theme");
        this.themeRepository.save(this.theme);

        this.user = new User("user", "user");
        this.userRepository.save(this.user);


        this.vote = new Vote(3, this.theme, this.user);
        this.voteRepository.save(this.vote);
    }

    @Test
    public void testFindAllByTheme(){
        List<Vote> votes = this.voteRepository.findAllByTheme(this.theme);
        assertEquals(votes.get(0), this.vote);
    }

    @Test
    public void testFindAllByUser(){
        List<Vote> votes = this.voteRepository.findAllByUser(this.user);
        assertEquals(votes.get(0), this.vote);
    }

    @Test
    public void testDeleteAllByTheme(){
        this.voteRepository.deleteAllByTheme(this.theme);
        List<Vote> votes = this.voteRepository.findAllByTheme(this.theme);
        assertEquals(votes.size(), 0);
    }

    @Test
    public void testDeleteAllByUser(){
        this.voteRepository.deleteAllByUser(this.user);
        List<Vote> votes = this.voteRepository.findAllByUser(this.user);
        assertEquals(votes.size(), 0);
    }

    @After
    public void delete(){
        this.themeRepository.delete(this.theme);
        this.userRepository.delete(this.user);
        this.voteRepository.delete(this.vote);
    }


}
